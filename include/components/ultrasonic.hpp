#ifndef ULTRASONIC_HPP
#define ULTRASONIC_HPP

/**
 * we are using the newping library for ultrasonic comms
 * This is all skeltal code, IE, just a general idea of how to implement this
 */
namespace components {
    class Ultrasonic 
    {
    public:
        Ultrasonic(int trigger_pin, int echo_pin, int max_distance_cm);
        // returns time of flight in ms
        unsigned long ping();
        // not ms
        unsigned long ping_cm();
        
        
    private:
        int m_trigger_pin;
        int m_echo_pin;

    };
}
#endif