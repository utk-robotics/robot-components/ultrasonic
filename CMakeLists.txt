cmake_minimum_required(VERSION 3.1)
project(roboclaw)

# Will add testing in the future
# set(gtest_force_shared_crt TRUE CACHE BOOL "")
# add_subdirectory(test)
set(CMAKE_EXPORT_COMPILE_COMMANDS ON)

option(ENABLE_TESTING "Enable Testing" OFF)
add_subdirectory(ext)
file(GLOB_RECURSE ${PROJECT_NAME}_SOURCES "src/*.cpp")
file(GLOB_RECURSE ${PROJECT_NAME}_HEADERS "include/*.hpp")

# Get all .cpp files except for main.cpp
add_library(${PROJECT_NAME} STATIC ${${PROJECT_NAME}_SOURCES} ${${PROJECT_NAME}_HEADERS})
set_property(TARGET ${PROJECT_NAME} PROPERTY CXX_STANDARD 11)
target_include_directories(${PROJECT_NAME} PUBLIC include)
target_link_libraries(${PROJECT_NAME} EmbMessengerHost RoboClaw)

# TODO: Add testing
# if(ENABLE_TESTING)

# endif()
